import * as E from 'fp-ts/lib/Either'

const toEither = (a: number): E.Either<string, number> =>
  a !== 0 ? E.right(a) : E.left('value cannnot be 0')

const unwrapOrError: (ma: E.Either<string, number>) => string = E.match(
  (err: string) => `error is ${err}`,
  (value: number) => value.toString()
)

console.log(unwrapOrError(toEither(1))) // => '1'
console.log(unwrapOrError(toEither(0))) // => 'error is value cannot be 0'