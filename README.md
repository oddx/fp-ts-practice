# FP-TS-PRACTICE
A lil typescript repo for storing my more monadic ponderings

#### Rapid Dev
For a quick feedback loop, run `cd src/{dir} && bun run rapid` in the concept directory of your interest.
This will run the program every time the file changes.

#### Supplementary Reading
I referenced [this](https://inato.github.io/fp-ts-cheatsheet/Details.html) document in the unwrapping of my eithers